package com.tringleprogram;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

public class DrawTringle extends View {

    Paint paint = new Paint();
    int side1,side2,side3;

    public DrawTringle(Context context,int a, int b,int c) {
        super(context);
        side1 = a;
        side2 = b;
        side3 = c;
        init();
    }

    public DrawTringle(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawTringle(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        paint.setColor(Color.BLUE);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        CheckTriangle(canvas, side1, side2, side3);
    }

    public void CheckTriangle(Canvas canvas, int side1, int side2, int side3) {

        if (side3 < (side1 + side2) || side2 < (side1 + side3) || side1 < (side2 + side3)) {

            if (side1 == side2 && side2 == side3) {

                Toast.makeText(getContext(), "Equilateral Triangle", Toast.LENGTH_SHORT).show();
                drawEquilateralTriangle(canvas, 100);
            }else {
                if (side1 * side1 + side2 * side2 == side3 * side3 || side2 * side2 + side3 * side3 == side1 * side1 || side1 * side1 + side3 * side3 == side2 * side2) {

                    Toast.makeText(getContext(), "Right Angle Triangle", Toast.LENGTH_SHORT).show();
                    drawRightAngleTriangle(canvas, 100);
                }else {
                    if (side1 == side2 || side2 == side3 || side3 == side1) {

                        Toast.makeText(getContext(), "Isosceles Triangle", Toast.LENGTH_SHORT).show();
                        drawIsoscalesTriangle(canvas, 100);
                    } else {
                        Toast.makeText(getContext(), "Scelen Traingle", Toast.LENGTH_SHORT).show();
                        drawScalenTriangle(canvas, 100);
                    }
                }
            }


        }
    }

    private void drawScalenTriangle(Canvas canvas, int width) {
        Point p1 = new Point(), p2, p3;
        p1.x = 200;
        p1.y = 200;

        p2 = new Point(p1.x - (width), p1.y + (width / 2));
        p3 = new Point(p1.x - (width / 2), p1.y - (width));

        canvas.drawPath( drawTriangle(p1,p2,p3), paint);
    }

    private Path drawTriangle(Point p1, Point p2, Point p3) {
        Path path = new Path();
        path.moveTo(p1.x, p1.y);
        path.lineTo(p2.x, p2.y);
        path.lineTo(p3.x, p3.y);
        path.close();
        return path;
    }

    private void drawRightAngleTriangle(Canvas canvas, int width) {
        Point p1 = new Point(), p2, p3;
        p1.x = 100;
        p1.y = 100;
        p2 = new Point(p1.x + width, p1.y);
        // p3 = new Point(p1.x + (width), p1.y - width);
        p3 = new Point(p1.x , p1.y-width );
        canvas.drawPath(drawTriangle(p1,p2,p3), paint);
    }


    public void drawEquilateralTriangle(Canvas canvas, int width) {
        Point p1 = new Point(), p2, p3;
        p1.x = 100;
        p1.y = 100;

// Equilateral Triangle

        p2 = new Point(p1.x + width, p1.y);
        p3 = new Point(p1.x + (width / 2), p1.y - width);
        Path path = new Path();


        canvas.drawPath(drawTriangle(p1,p2,p3), paint);
    }

    public void drawIsoscalesTriangle(Canvas canvas, int width) {
        Point p1 = new Point(), p2, p3;
        p1.x = 100;
        p1.y = 100;

        p2 = new Point(p1.x - (width), p1.y + (width));
        p3 = new Point(p1.x + width, p1.y + width);
        canvas.drawPath(drawTriangle(p1,p2,p3), paint);
    }
}
