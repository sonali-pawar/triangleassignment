 package com.tringleprogram.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tringleprogram.DrawTringle;
import com.tringleprogram.R;

import org.xmlpull.v1.XmlPullParser;


 public class TriangleFragment extends Fragment {
public static final String fragmentname = "TriangleFragment";
    public static TriangleFragment newInstance(Bundle bundle) {
        TriangleFragment fragment = new TriangleFragment();
        Bundle args = new Bundle();
        args.putString(fragmentname,"TriangleFragment");
        args.putInt("side1",bundle.getInt("side1"));
        args.putInt("side2",bundle.getInt("side2"));
        args.putInt("side3",bundle.getInt("side3"));
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        int a = getArguments().getInt("side1");
        int b = getArguments().getInt("side2");
        int c = getArguments().getInt("side3");

        DrawTringle drawView = new DrawTringle(getContext(),a,b,c);
        drawView.setBackgroundColor(Color.WHITE);
        View rootView = drawView;
        //View rootView = inflater.inflate(R.layout.fragment_triangle,container,false);

        return rootView;
    }


}
