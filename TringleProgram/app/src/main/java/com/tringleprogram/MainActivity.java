package com.tringleprogram;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.tringleprogram.fragment.TriangleFragment;

public class MainActivity extends AppCompatActivity {

    public static final String fragmentname = "TriangleFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.showTriangle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = getInput();
                Fragment fragment = TriangleFragment.newInstance(bundle);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName()).addToBackStack(fragmentname).commit();
            }
        });

    }

    private Bundle getInput() {
        Bundle bundle = new Bundle();
        EditText et1 = (EditText) findViewById(R.id.editText1);
        EditText et2 = (EditText) findViewById(R.id.editText2);
        EditText et3 = (EditText) findViewById(R.id.editText3);


        bundle.putInt("side1", Integer.parseInt(et1.getText().toString()));
        bundle.putInt("side2", Integer.parseInt(et2.getText().toString()));
        bundle.putInt("side3", Integer.parseInt(et3.getText().toString()));
        return bundle;
    }

}
